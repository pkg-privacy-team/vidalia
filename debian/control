Source: vidalia
Section: net
Priority: extra
Maintainer: Debian Privacy Tools Maintainers <pkg-privacy-maintainers@lists.alioth.debian.org>
Uploaders: intrigeri <intrigeri@debian.org>, Ulises Vitulli <dererk@debian.org>
Build-Depends: debhelper (>= 6.0.7~), dh-apparmor, autotools-dev, libqt4-dev, cmake, libssl-dev, dpkg-dev (>= 1.16.1.1~)
Standards-Version: 3.9.5
Homepage: https://www.torproject.org/projects/vidalia
Vcs-Git: https://anonscm.debian.org/git/pkg-privacy/packages/vidalia.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-privacy/packages/vidalia.git

Package: vidalia
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, debconf | debconf-2.0,
 ucf, adduser, tor
Suggests: apparmor
Description: controller GUI for Tor
 Vidalia offers a graphical user interface to start and stop Tor, view
 its status at a glance, and monitor its bandwidth usage.
 .
 Vidalia also makes it easy to contribute to
 the Tor network by helping you set up a Tor server.
 .
 Tor protects privacy in communications via a distributed network of
 relays run by volunteers all around the world: it prevents anybody
 watching Internet connections from learning what sites you visit,
 and it prevents the visited sites from learning your physical
 location. Tor works with web browsers, instant messaging programs,
 remote login clients, and many other TCP-based applications.
