vidalia (0.2.21-5) unstable; urgency=medium

  * Put under the umbrella of the Anonymity Tools Debian Maintainers team.
  * AppArmor: allow Vidalia to browse directories up to /{var/,}run/tor,
    that contains the control port authentication cookie by default.
  * Adjust-filename-of-the-cookie-authentication-file-to.patch:
    new patch that adjusts the filename of the Tor cookie authentication file
    to match Debian's default.
  * Update README.Debian to match default location of the Tor authentication
    cookie.

 -- intrigeri <intrigeri@debian.org>  Thu, 14 Aug 2014 11:32:11 +0200

vidalia (0.2.21-4) unstable; urgency=medium

  * Add Turkish translation of debconf messages (Closes: #757501).
    Thanks to Mert Dirik <mertdirik@gmail.com> for the patch!
  * Drop Suggests on iceweasel-torbutton, that was removed from Debian.

 -- intrigeri <intrigeri@debian.org>  Thu, 14 Aug 2014 09:50:50 +0200

vidalia (0.2.21-3) unstable; urgency=medium

  * Disable miniupnpc support: it uses a very old embedded copy of that library.
  * Remove spurious trailing whitespace in debian/rules.

 -- intrigeri <intrigeri@debian.org>  Mon, 24 Feb 2014 11:48:47 +0100

vidalia (0.2.21-2) unstable; urgency=medium

  * Import Japanese translation of debconf templates,
    thanks to victory <victory.deb@gmail.com>. Closes: #718999.
  * Import Polish translation of debconf templates,
    thanks to Magdalena Zofia Kubot <magdalena.kubot@gmail.com>.
    Closes: #731336.
  * AppArmor profile improvements:
    - allow reading same-owner /proc/PID/cmdline
    - allow reading same-owner /proc/PID/fd/
    - allow access to the session's D-Bus
    - allow reading system-wide GTK themes (Closes: #682431)
  * Don't version dependencies that are satisfied on oldstable (Squeeze).
  * Use canonical URL for Vcs-*.
  * Declare compatibility with standards version 3.9.5.
  * Drop obsolete Lintian override.

 -- intrigeri <intrigeri@debian.org>  Tue, 18 Feb 2014 10:19:08 +0100

vidalia (0.2.21-1) unstable; urgency=low

  [ Ulises Vitulli ]
  * Un-hold locked new upstream release: fixes several bugs:
    - Avoid waiting to reload router list if ns/all returned empty.
    - Stop trying to load router information if torControl is not connected.
    - Fix RouterDescriptor's bandwidth and uptime values when defined from the
      consensus.
    - Save ControlPortWriteToFile using relative path.
  * Updated Debconf template translations:
    - Italian, thanks Fabio Pirola (Closes: #699082).
  * Updated Standard-version to 3.9.4 (no changes needed).

  [ intrigeri ]
  * Drop the patches that were taken from upstream 0.2.21:
    - Fix-wrong-uptime-bandwidth-in-relay-list.patch
    - Populate-the-relay-list-at-startup.patch

 -- Ulises Vitulli <dererk@debian.org>  Sat, 11 May 2013 11:07:13 -0300

vidalia (0.2.20-2) unstable; urgency=low

  * New patches cherry-picked from upstream 0.2.21:
    - Fix-wrong-uptime-bandwidth-in-relay-list.patch (Closes: #699178)
      This is upstream commit b3d4f7f with the changes/* file removed.
    - Populate-the-relay-list-at-startup.patch (Closes: #699179)
      This is taken from upstream commit b00f51dc, but with the fix for
      upstream Trac#6482 (that doesn't seem worth a freeze exception) and
      the changes/* file removed.

 -- intrigeri <intrigeri@debian.org>  Sat, 02 Feb 2013 17:02:01 +0100

vidalia (0.2.20-1) unstable; urgency=low

  [ Ulises Vitulli ]
  * New upstream release, fixes several bugs:
    - Append router status information if Tor is using microdescriptors, this
      bug produced empty nodes lists and their interactions (Closes: #680489).
    - Display paths with their native separators. (Fixes upstream Trac#4337).
    - Do not ignore the Show on Startup checkbox. (Fixes upstream Trac#5351).
    - Do not add relative root to DataDirectory if it is empty. 
      (Fixes upstream Trac#6178).

  [ intrigeri ]
  * AppArmor profile: add a missing credential, allow Vidalia
    to write its own configuration (Closes: #681365).

 -- Ulises Vitulli <dererk@debian.org>  Fri, 13 Jul 2012 20:20:45 -0300

vidalia (0.2.19-2) unstable; urgency=low

  * Moved apparmor from runtime dep to a suggested one (Closes: #678549).
  * Clarify apparmor current Wheezy state at README.Debian (Closes: #678550).
  * Added intrigeri as Uploader.

 -- Ulises Vitulli <dererk@debian.org>  Sat, 23 Jun 2012 19:00:41 -0300

vidalia (0.2.19-1) unstable; urgency=low

  [ intrigeri ]
  * Imported Upstream version 0.2.19
  * Install AppArmor profile.
  * Enable hardening flags.

  [ Ulises Vitulli ]
  * Added runtime dependency on apparmor.
  * Updated d/NEWS and d/README.Debian to reflect new features.
  * Move dirs from d/rules to d/vidalia.dir.
  * Updated Standard-version to 3.9.3 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Fri, 22 Jun 2012 07:18:44 -0300

vidalia (0.2.17-1) unstable; urgency=low

  * New upstream release.
  * Updated debian/NEWS with a known release point bug.
  * Updated debian/README.Debian documentation (Closes: #654642).
  * Debconf templates improves, translation round called:
    - Czech, thanks Martin.Šín (Closes: #654709).
    - Danish, thanks Joe.Dalton (Closes: #654820).
    - Russian, thanks Yuri.Kozlov (Closes: #654842).
    - Portuguese, thanks Miguel.Figueiredo (Closes: #654981).
    - Swedish, thanks Martin.Bagge (Closes: #655046). 
    - Dutch, thanks Jeroen.Schot (Closes: #655319).
    - French, thanks Christian.Perrier (Closes: #655546).
    - German, thanks Martin.E.Schauer (Closes: #656123).
    - Indonesian, new translation added, thanks Mahyuddin.Susanto 
      (Closes: #658705). 
    - Simplified and Traditional Chinese, thanks Vern.Sun.
    - Spanish.
  * Updated Maintainer/Uploader fields.

 -- Ulises Vitulli <dererk@debian.org>  Sat, 11 Feb 2012 22:26:58 -0300

vidalia (0.2.15-1) unstable; urgency=low

  * New upstream release.

 -- Ulises Vitulli <dererk@debian.org>  Tue, 15 Nov 2011 17:14:07 -0300

vidalia (0.2.14-3) unstable; urgency=low

  * Moved debconf actions from config to postinst script (Closes: #641044).
  * Remove deprecated files that enforced Tor not to start after having
    chosen to do so. This comes from the older approach to control Tor
    (Closes: #642223).
  * Added debconf templates translations:
    - French, thanks Christian Perrier (Closes: #640769).
    - German, thanks Martin.E.Schauer (Closes: #641920).

 -- Ulises Vitulli <dererk@debian.org>  Mon, 19 Sep 2011 12:05:30 -0300

vidalia (0.2.14-2) unstable; urgency=low

  * The 'Napoleon-Of-Crime' dupload.
  * Tor's ControlSocket enabled version upload allowed to port exp 
    changes to handle socket com out of the box (Closes: #640077).

 -- Ulises Vitulli <dererk@debian.org>  Sat, 03 Sep 2011 15:47:23 -0300

vidalia (0.2.14-1) unstable; urgency=low

  * The 'Monday-Rocking-Skiing' dupload.
  * New upstream release. 
  * Improve building targets for simplifying portscripts.

 -- Ulises Vitulli <dererk@debian.org>  Mon, 29 Aug 2011 09:43:47 -0300

vidalia (0.2.12-2) unstable; urgency=low

  * The 'It-is-a-nice-day-for-barbacue' dupload.
  * Debconf kills Tor daemon while told not to (Closes: #623203).
  * Missing steps on CookieAuthentication/ControlPort documentation
    (Closes: #623309).

 -- Ulises Vitulli <dererk@debian.org>  Tue, 19 Apr 2011 12:26:26 -0300

vidalia (0.2.12-1) unstable; urgency=low

  * The 'Long-time-no-see!' dupload.
  * New upstream release.
  * Updated Homepage field.
  * Updated Standard-version to 3.9.2.0 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Mon, 11 Apr 2011 11:20:42 -0300

vidalia (0.2.10-3) unstable; urgency=low

  * An 'Oops-ish' dupload.
  * Fixing error while purging pkg (Closes: #600744).
  * Improved pkg documentation and proper l10n English review.

 -- Ulises Vitulli <dererk@debian.org>  Tue, 19 Oct 2010 17:03:46 -0300

vidalia (0.2.10-2) unstable; urgency=low

  * Improved debconf templates interaction and set a default answer case
    (Closes: #592773, #598240, #599050, #599141).
  * Update Standard-version to 3.9.1 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Fri, 24 Sep 2010 11:18:20 -0300

vidalia (0.2.10-1) unstable; urgency=low

  * New upstream release.

 -- Erinn Clark <erinn@torproject.org>  Thu, 09 Sep 2010 01:21:19 -0700

vidalia (0.2.9-2) unstable; urgency=low

  * Cleaning unrequired build-deps, since we know use quilt source format.

 -- Ulises Vitulli <dererk@debian.org>  Mon, 31 May 2010 18:52:47 -0300

vidalia (0.2.9-1) unstable; urgency=low
 
  [ Vern Sun ]
  * New lintian addition "missing-debian-source-format", Switch package
    format to 3.0 (quilt).
  * Add lintian override about unused-debconf-template which are used via
    variables $question.
  * Updated Build-Depends: debhelper (>= 6.0.7~) (for dh_lintian).

  [ Ulises Vitulli ]
  * New upstream release.
  * The 'Cowabung' upload.
  * Reviewed Debconf templates by l10n Team (Closes: #579715),
    Thanks Christian Perrier:
    - Portuguese, thanks Américo Monteiro (Closes: #579767).
    - Czech, thanks Martin Sin (Closes: #579785).
    - Russian, thanks Yuri Kozlov (Closes: #579818).
    - Swedish, thanks Martin Bagge (Closes: #580138).
    - Italian, thanks Vincenzo Campanella (Closes: #580178).
    - French, thanks Christian Perrier (Closes: #580612).
    - German, thanks Martin.E.Schauer (Closes: #580903).
    - Spanish, thanks Francisco Javier Cuadrado (Closes: #581492).
    - Simplified Chinese, thanks Simplified Chinese Team (Closes: #581563).

  * New debconf translations:
    - Danish, thanks Joe Hansen (Closes: #580123).
    - Vietnamese, thanks Clytie Siddall (Closes: #581532).

  * Cleaned patching target, we now handle them through 3.0 source format.
  * Cleaned repackaging target, we don't have to deal with them anylonger.
  * Updated some files missing on debian/copyright.
  * Improved package description and proper l10n.

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Thu, 20 May 2010 21:38:33 -0300

vidalia (0.2.8-1) unstable; urgency=low

  * New upstream release.
  * New debconf translations:
    - Portuguese (Closes: #577000).
    - Russian (Closes: #577162).

 -- Erinn Clark <erinn@torproject.org>  Sat, 17 Apr 2010 18:54:45 -0700

vidalia (0.2.7-3) unstable; urgency=low

  * Fix broken config script.

 -- Erinn Clark <erinn@torproject.org>  Mon, 05 Apr 2010 02:56:03 -0700

vidalia (0.2.7-2) unstable; urgency=low

  * Add option to debconf question to disable Tor on boot in concert with Tor's
    recent sourcing of /etc/default/tor.vidalia. (Closes: #542671)

 -- Erinn Clark <erinn@torproject.org>  Sun, 04 Apr 2010 20:16:07 -0700

vidalia (0.2.7-1) unstable; urgency=low

  * New upstream release (Closes: #566931).
    - Droping pkg from experimental, now going to unstable.
    - Drop 0.1.x stable branch in favor of 0.2.x, due to devel matureness.
    - Removing kFreeBSD-gcc4.4-fix.patch. Patch accepted and applied on
      upstream side before this package to be uploaded (check #566931).
    - Added upstream uploader into the about-to-dissappear repacking
      source target gpg keyring ('source-repkg').
  * The 'All systems are go!' upload.

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Sun, 31 Jan 2010 17:11:20 -0300

vidalia (0.2.6-1) unstable; urgency=low

  * New upstream release.

 -- Erinn Clark <erinn@torproject.org>  Fri, 15 Jan 2010 16:14:58 -0800

vidalia (0.2.5-1) experimental; urgency=low

  * Upstream development release:
   o Add support in the Network settings page for configuring the
     Socks4Proxy and Socks5Proxy* options added on Tor 0.2.2.1-alpha.
   o Add a context menu for highlighted event items in the "Basic" message
     log view that allows the user to copy the selected item text to the
     clipboard.
   o Maybe fix a time conversion bug that could result in Vidalia
     displaying the wrong uptime for a relay in the network map.
   o Stop trying to enforce proper quoting and escaping of arguments to be
     given to the proxy executable.

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Tue, 27 Oct 2009 14:16:32 -0300

vidalia (0.2.4-2) experimental; urgency=low

  * Added repackaging from source target functionalty on debian scripts
   - README.Source
   - debian/rules
  * Fix bug which points to a non running Tor process

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Mon, 19 Oct 2009 19:36:29 -0300

vidalia (0.2.4-1) experimental; urgency=low

  * Upstream development branch sync. 

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Sat, 12 Sep 2009 09:26:18 -0300

vidalia (0.2.0-1) experimental; urgency=low

  * Upload of development branch.

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Thu, 23 Jul 2009 22:48:05 -0300

vidalia (0.1.15-2) unstable; urgency=low

  * Added repackaging from source target functionalty on debian scripts
   - README.Source
   - debian/rules
  * Fix bug which points to a non running Tor process (Closes: #542670).

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Mon, 19 Oct 2009 19:20:36 -0300

vidalia (0.1.15-1) unstable; urgency=low

  * New upstream release.
  * Debconf translations:
    - Czech (Closes: #535973).
  * Really fix debconf script issue.
  * Update Standard-version to 3.8.2 (no changes needed)

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Tue, 21 Jul 2009 06:00:49 -0300

vidalia (0.1.13-3) unstable; urgency=low

  * Fixing debconf scripts issue that output twice the same message.

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Thu, 11 Jun 2009 00:48:57 -0300

vidalia (0.1.13-2) unstable; urgency=low

  * Update Upstream URL on debian/watch

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Tue, 09 Jun 2009 22:05:58 -0300

vidalia (0.1.13-1) unstable; urgency=low

  [ Vern Sun ]
  * New upstream release.
  * Add Debconf zh_TW.po.

  [ Ulises Vitulli ]
  * Force scripts to die in case something goes wrong:
    - debian/config
    - debian/postinst
  * Remove deprecated directive dh_desktop.

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Wed, 03 Jun 2009 20:11:57 -0300

vidalia (0.1.12-1) unstable; urgency=low

  [ Vern Sun ]
  * New upstream release

  [ Ulises Vitulli ]
  * debian/watch: avoid evaluating development releases.
  * debian/control: updated to 3.8.1 Policy.

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Sat, 11 Apr 2009 11:00:18 -0300

vidalia (0.1.11-1) unstable; urgency=low

  * New upstream release.
  * Added README.source.

 -- Vern Sun <s5unty@gmail.com>  Wed, 25 Feb 2009 11:55:46 +0800

vidalia (0.1.10-1) unstable; urgency=low
   
  [ Vern Sun ]
  * New upstream release.
  * Debconf translations:
    - Swedish (Closes: #508751)
  
  [ Ulises Vitulli ]
  * DQT_LRELEASE_EXECUTABLE and QT_LUPDATE_EXECUTABLE pointing to qt4 libs
     avoids building conflicts if alternatives are pointing to qt3 ones.
  * Applying temporal patch pointing to default Debian Tor's binary.
  * Little pkg modifications:
    - Added Vidalia's FAQ licence
    - Moved .xpm and .desktop to vidalia.install.
    - Added Description on quilt patch
    - Remove 'Encoding' field on vidalia.desktop

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Wed, 14 Jan 2009 14:02:06 -0200

vidalia (0.1.9-3) unstable; urgency=low

  * Fixing missing copyright holders.

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Mon, 27 Oct 2008 12:20:26 -0200

vidalia (0.1.9-2) unstable; urgency=low

  * Moved Pre-depens with debconf to Depends

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Sun, 28 Sep 2008 20:04:44 +0200

vidalia (0.1.9-1) unstable; urgency=low

  [ Vern Sun ]
  * Initial release (Closes: #405431).

  [ Ulises Vitulli ]
  * Added README.Debian and FAQ.txt.
  * Added menu and .desktop files.
  * Added vidalia.xpm icon (provided by upstream).
  * Debconf translations:
    - Spanish.
    - Catalan.
    - Russian.
    - Finnish.
    - Italian.
    - French.
    - German.
    - Portuguese.

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Sat, 20 Sep 2008 05:45:56 -0300
